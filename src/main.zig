const std = @import("std");
const heap = std.heap;
const gpa = heap.GeneralPurposeAllocator;

pub fn main() !void {
    const stdout_file = std.io.getStdOut().writer();
    var bw = std.io.bufferedWriter(stdout_file);
    const stdout = bw.writer();

    try stdout.print("Run `zig build test` to run the tests.\n", .{});

    try bw.flush(); // don't forget to flush!

    var alloc = std.heap.GeneralPurposeAllocator.init();
    defer alloc.deinit();
}
